# Populate

> Demographic and population needs and wants simulation software

---

## Usage

`ruby populate.rb --count 100`

Or to get the help,

`ruby populate.rb --help`
