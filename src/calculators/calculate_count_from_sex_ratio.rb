module Calculators
  class CalculateCountFromSexRatio
    def self.call(count:, sex_ratio:)
      sex_ratios = sex_ratio.split(':')
      raise 'Format is wrong for sex ratio, it should be number followed by semicolon and then number, e.g. 1:1' if sex_ratios.nil? || sex_ratios.length != 2

      ratio_left = sex_ratios[0].to_i
      ratio_right = sex_ratios[1].to_i

      first_half = (ratio_left/(ratio_left + ratio_right).to_f * count).ceil
      second_half = count - first_half
      {
        first_half: first_half,
        second_half: second_half,
      }
    end
  end
end
