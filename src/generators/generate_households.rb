require './src/math/random_gaussian'
require './src/math/get_nearest_even'

module Generators
  class GenerateHouseholds
    # Return how many people are in total in the households
    def self.sum_household_population(households)
      population_count = 0
      households.each do |household|
        if household[:household_type] == 'family'
          population_count += household[:mother] if household[:mother]
          population_count += household[:father] if household[:father]
          population_count += household[:children] if household[:children]
        else
          population_count += 1
        end
      end
      population_count
    end

    def self.generate_families(count:, family_template:, mean_children_count:)
      families = []
      random_count = Math::RandomGaussian.new(mean_children_count, 1)
      # TODO: Generate households until
      i = 0
      while i < count
        break if i == count
        children = random_count.rand.ceil
        children = children > 0 ? children : 1
        family = family_template.clone
        family[:children] = children
        i += family[:mother] + family[:father] + family[:children]
        family[:children] -= (i - count) if i > count
        break if family[:children] < 1
        families += [family]
      end
      families
    end

    # Defaults based on US Census: https://www.census.gov/newsroom/press-releases/2016/cb16-192.html
    def self.call(
      count:,
      mean_children_count: 2,
      single_household_percentage: 25,
      childless_household_percentage: 48,
      single_mother_household_percentage: 23,
      single_father_household_percentage: 4,
      both_parent_household_percentage: 69,
      no_parent_household_percentage: 3)
      # TODO: Figure out multi-surname households
      # TODO: Figure out multi-generation households
      # TODO: Make this waaaay less heteronormative and inclusive of LGBT households
      households = []
      single_household_count = (count * (single_household_percentage.to_f/100)).round
      households += [{household_type: 'non-family'}] * single_household_count

      childless_household_count_unadjusted = ((count - single_household_count) * (childless_household_percentage.to_f/100)).round
      childless_household_count_adjusted = Math::GetNearestEven.call(childless_household_count_unadjusted)
      households += [{household_type: 'family', mother: 1, father: 1}] * (childless_household_count_adjusted/2)

      family_count = count - single_household_count - childless_household_count_adjusted
      family_count_tally = 0

      both_parent_household_count = (family_count * (both_parent_household_percentage.to_f/100)).round
      households += generate_families(
        count: both_parent_household_count,
        family_template: {household_type: 'family', mother: 1, father: 1},
        mean_children_count: mean_children_count,
      )

      family_count_tally += both_parent_household_count

      single_mother_household_count = (family_count * (single_mother_household_percentage.to_f/100)).round
      households += generate_families(
        count: single_mother_household_count,
        family_template: {household_type: 'family', mother: 1, father: 0},
        mean_children_count: mean_children_count,
      ) if family_count_tally < family_count

      family_count_tally += single_mother_household_count

      single_father_household_count = (family_count * (single_father_household_percentage.to_f/100)).round
      households += generate_families(
        count: single_father_household_count,
        family_template: {household_type: 'family', mother: 0, father: 1},
        mean_children_count: mean_children_count,
      ) if family_count_tally < family_count

      family_count_tally += single_father_household_count

      no_parent_household_count = (family_count * (no_parent_household_percentage.to_f/100)).round
      households += generate_families(
        count: no_parent_household_count,
        family_template: {household_type: 'family', mother: 0, father: 0},
        mean_children_count: mean_children_count,
      ) if family_count_tally < family_count

      family_count_tally += no_parent_household_count

      population_count = sum_household_population(households)
      # Pad with single households to get the right count
      households += [{household_type: 'non-family'}] * (count - population_count)
      households.shuffle
    end
  end
end
