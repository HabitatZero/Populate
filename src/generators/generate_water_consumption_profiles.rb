module Generators
  class GenerateWaterConsumptionProfiles
    def self.generate_water_consumption_profile(
      count:,
      type:,
      consumption_baseline:
    )
      profiles = []
      (1..count).each do
        profiles += [
          {
            type: type,
            daily_consumption: consumption_baseline + rand(-40..40)
          }
        ]
      end
      profiles
    end

    def self.call(
      count:, 
      frugal_percentage: 10,
      wasteful_percentage: 10,
      average_consumption: 300,
      frugal_consumption: 250,
      wasteful_consumption: 350)
      average_percentage = 100 - frugal_percentage - wasteful_percentage
      average_percentage = average_percentage * -1 if average_percentage < 0

      average_count = (count * (average_percentage/100.to_f)).round
      frugal_count = (count * (frugal_percentage/100.to_f)).round
      wasteful_count = (count * (wasteful_percentage/100.to_f)).round

      water_consumption_profiles = []
      
      water_consumption_profiles += generate_water_consumption_profile(
        count: average_count,
        type: 'average',
        consumption_baseline: average_consumption,
      ) if average_count > 0

      water_consumption_profiles += generate_water_consumption_profile(
        count: frugal_count,
        type: 'frugal',
        consumption_baseline: frugal_consumption,
      ) if frugal_count > 0

      water_consumption_profiles += generate_water_consumption_profile(
        count: wasteful_count,
        type: 'wasteful',
        consumption_baseline: wasteful_consumption,
      ) if wasteful_count > 0

      water_consumption_profiles = water_consumption_profiles.shuffle

      # Adjust to meet count
      if count > water_consumption_profiles.length
        water_consumption_profiles += generate_water_consumption_profile(
          count: count - water_consumption_profiles.length,
          type: 'average',
          consumption_baseline: average_consumption,
        )
      elsif water_consumption_profiles.length > count
        water_consumption_profiles.slice!(0, water_consumption_profiles.length - count)
      end
      water_consumption_profiles
    end
  end
end