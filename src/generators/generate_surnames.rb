
require 'sequel'
require 'pathname'

module Generators
  class GenerateSurnames
    def self.call(count: 1)
      db_path = Pathname.new('data/db/populate.sl3').realdirpath.to_s
      db = Sequel.sqlite(db_path)

      sql = <<~SQL
        SELECT name
        FROM last_names
        ORDER BY random()
        LIMIT :count
      SQL

      db.fetch(sql, count: count).to_a
    end
  end
end
