require 'sequel'
require 'pathname'

module Generators
  class GenerateFirstNames
    def self.call(count:, gender: nil)
      db_path = Pathname.new('data/db/populate.sl3').realdirpath.to_s
      db = Sequel.sqlite(db_path)

      gender_clause = gender ? "WHERE gender IS :gender" : ''

      sql = <<~SQL
        SELECT
          name,
          CASE WHEN gender = 'M' THEN 'male' ELSE 'female' END AS gender
        FROM first_names
        #{gender_clause}
        ORDER BY random()
        LIMIT :count
      SQL

      db.fetch(sql, count: count, gender: gender).to_a
    end
  end
end
