require 'sequel'
require 'pathname'

module Generators
  class GenerateNames
    def self.call(count:, gender: nil, household: false)
      db_path = Pathname.new('data/db/populate.sl3').realdirpath.to_s
      db = Sequel.sqlite(db_path)

      gender_clause = gender ? "WHERE gender IS :gender" : ''

      first_name_sql = <<~SQL
        SELECT name, gender
        FROM first_names
        #{gender_clause}
        ORDER BY random()
        LIMIT :count
      SQL

      last_name_sql = <<~SQL
        SELECT name
        FROM last_names
        ORDER BY random()
        LIMIT :count
      SQL

      first_names = db.fetch(first_name_sql, count: count, gender: gender).to_a
      last_names = if household
        db.fetch(last_name_sql, count: 1).to_a
      else
        db.fetch(last_name_sql, count: count).to_a
      end

      full_names = []
      first_names.each_with_index do |first_name, index|
        last_name = if household
          last_names[0][:name].capitalize
        else
          last_names[index][:name].capitalize
        end
        full_names << {
          first_name: first_name[:name],
          last_name: last_name,
          gender: first_name[:gender],
        }
      end

      full_names
    end
  end
end
