module Generators
  class GenerateAgeForStage
    def self.call(stage)
      if stage == 'infant'
        rand(0..4)
      elsif stage == 'child'
        rand(5..9)
      elsif stage == 'adolescent'
        rand(10..19)
      elsif stage == 'young_adult'
        rand(20..29)
      elsif stage == 'adult'
        rand(30..39)
      elsif stage == 'early_middle_age'
        rand(40..49)
      elsif stage == 'late_middle_age'
        rand(50..65)
      elsif stage == 'senior'
        rand(66..99)
      end
    end
  end
end