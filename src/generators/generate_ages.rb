module Generators
  class GenerateAges
    def self.get_count_of_stage(count:, percentage:, total: 100)
      (count * (percentage/total.to_f)).round
    end

    def self.generate_ages_by_stage(count:, range:, sex:, stage:)
      return [] if count < 1
      ages = []
      (1..count).each do
        ages += [{
          sex: sex,
          age: rand(range),
          stage: stage,
        }]
      end
      ages
    end

    # Based on https://www.populationpyramid.net/canada/2017/
    def self.call(
      count:,
      female_infant_percentage: 2.6, # 0-4
      female_child_percentage: 2.7, # 5-9
      female_adolescent_percentage: 5.2, # 10-19
      female_young_adult_percentage: 6.6, # 20-29
      female_adult_percentage: 6.9, # 30-39
      female_early_middle_age_percentage: 6.4, #40-49
      female_late_middle_age_percentage: 10.6, #50-65
      female_senior_percentage: 9.2, #66-99
      male_infant_percentage: 2.7, # 0-4
      male_child_percentage: 2.8, # 5-9
      male_adolescent_percentage: 5.5, # 10-19
      male_young_adult_percentage: 6.9, # 20-29
      male_adult_percentage: 6.9, # 30-39
      male_early_middle_age_percentage: 6.4, #40-49
      male_late_middle_age_percentage: 10.7, #50-65
      male_senior_percentage: 7.6)
      return [] if count < 1

      ages = []

      early_middle_age_range = (40..49)
      late_middle_age_range = (50..65)
      senior_range = (66..99)

      stages = [
        {
          stage: 'infant',
          age_range: (0..4),
          female_percentage: female_infant_percentage,
          male_percentage: male_infant_percentage,
        },
        {
          stage: 'child',
          age_range: (5..9),
          female_percentage: female_child_percentage,
          male_percentage: male_child_percentage,
        },
        {
          stage: 'adolescent',
          age_range: (10..19),
          female_percentage: female_adolescent_percentage,
          male_percentage: male_adolescent_percentage,
        },
        {
          stage: 'young_adult',
          age_range: (20..29),
          female_percentage: female_young_adult_percentage,
          male_percentage: male_young_adult_percentage,
        },
        {
          stage: 'adult',
          age_range: (30..39),
          female_percentage: female_adult_percentage,
          male_percentage: male_adult_percentage,
        },
        {
          stage: 'early_middle_age',
          age_range: (40..49),
          female_percentage: female_early_middle_age_percentage,
          male_percentage: male_early_middle_age_percentage,
        },
        {
          stage: 'late_middle_age',
          age_range: (50..65),
          female_percentage: female_late_middle_age_percentage,
          male_percentage: male_late_middle_age_percentage,
        },
        {
          stage: 'senior',
          age_range: (66..99),
          female_percentage: female_senior_percentage,
          male_percentage: male_senior_percentage,
        },
      ]

      total = stages.sum { |stage| stage[:female_percentage] + stage[:male_percentage] }.round

      stages.each do |stage|
        ages += generate_ages_by_stage(
          count: get_count_of_stage(count: count, percentage: stage[:female_percentage], total: total),
          range: stage[:age_range],
          sex: 'female',
          stage: stage[:stage]
        )

        ages += generate_ages_by_stage(
          count: get_count_of_stage(count: count, percentage: stage[:male_percentage], total: total),
          range: stage[:age_range],
          sex: 'male',
          stage: stage[:stage]
        )
      end

      # Trim off the excess
      while ages.length > count
        ages.pop
      end

      ages
    end
  end
end
