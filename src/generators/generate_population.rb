require './src/generators'
require './src/calculators'

module Generators
  class GeneratePopulation
    def self.pick_from_ages(ages:, sex:, stages:)
      age_index = ages.find_index { |age| stages.include? age[:stage] && sex == age[:sex]}
      if age_index.nil?
        # Randomly pick a stage from the list
        Generators::GenerateAgeForStage.call(stages.shuffle.first)
      else
        ages.slice!(age_index)
      end
    end

    def self.call(settings)
      ratiod_count = Calculators::CalculateCountFromSexRatio.call(count: settings[:count], sex_ratio: settings[:sex_ratio])
      womenfolk_names = Generators::GenerateNames.call(count: ratiod_count[:second_half], gender: 'F')
      menfolk_names = Generators::GenerateNames.call(count: ratiod_count[:first_half], gender: 'M')
      diets = Generators::GenerateDiets.call(count: settings[:count])
      ages = Generators::GenerateAges.call(count: settings[:count]).shuffle
      water_consumption_profiles = Generators::GenerateWaterConsumptionProfiles.call(count: settings[:count])

      womenfolk = []
      womenfolk_names.each do |name|
        womenfolk += [{
          first_name: name[:first_name],
          last_name: name[:last_name],
          gender: 'female',
          diet: diets.pop,
          water_consumption_profile: water_consumption_profiles.pop,
        }]
      end

      menfolk = []
      menfolk_names.each do |name|
        menfolk += [{
          first_name: name[:first_name],
          last_name: name[:last_name],
          gender: 'male',
          diet: diets.pop,
          water_consumption_profile: water_consumption_profiles.pop,
        }]
      end

      households_with_persons = []
      households = Generators::GenerateHouseholds.call(count: settings[:count])
      family_names = Generators::GenerateSurnames.call(count: households.length)
      # First pass to fill in the households, second to fill in the children
      households.each do |household|
        if household[:household_type] == 'family'
          surname = family_names.pop[:name].capitalize
          family = {
            family_name: "The #{surname}",
            family_surname: surname,
            family_members: []
          }
          # TODO: Handle fathers that change their names, or mothers that don't, as a percentage
          if household[:father]
            person = menfolk.pop
            person[:last_name] = surname
            person[:marital_status] = 'married'
            person[:family_role] = 'father'
            family[:family_members] += [{person: person}]
          end

          if household[:mother]
            person = womenfolk.pop
            person[:last_name] = surname
            person[:marital_status] = 'married'
            person[:family_role] = 'mother'
            family[:family_members] += [{person: person}]
          end
        else
          person = (rand(0..1) == 0) ? menfolk.pop : womenfolk.pop
          person[:marital_status] = 'single'
          person[:family_role] = 'single'
          person[:age] = pick_from_ages(ages: ages, sex: person[:gender], stages: ['young_adult', 'adult', 'early_middle_age', 'late_middle_age', 'senior'])
          family = {
              family_name: "The #{person[:last_name]}",
              family_members: [{person: person}]
            }
        end
        households_with_persons += [family]
      end

      # Second pass to fill in the children with the remainder of the persons generated
      households.each_with_index do |household, index|
        if household[:children]
          (1..household[:children]).each do |i|
            random_decider = rand(0..1)
            if menfolk.length <= 0 && womenfolk.length <= 0
              first_name = Generators::GenerateFirstNames.call(count: 1).first
              person = {
                first_name: first_name[:name],
                last_name: households_with_persons[index][:family_surname],
                diet: 'omnivore',
                gender: first_name[:gender],
                water_consumption_profile: water_consumption_profiles.length > 0 ? water_consumption_profiles.pop : Generators::GenerateWaterConsumptionProfiles.call(count: 1).first,
              }
            else
              person = if random_decider == 0
                menfolk.empty? ? womenfolk.pop : menfolk.pop
              else
                womenfolk.empty? ? menfolk.pop : womenfolk.pop
              end
            end
            person[:last_name] = households_with_persons[index][:family_surname]
            person[:marital_status] = 'single'
            person[:family_role] = 'child'
            households_with_persons[index][:family_members] += [{ person: person }]
          end
        end
      end

      # Third pass to assign ages
      households_with_persons.each do |household|
        children = household[:family_members].find_all do |family_member|
          family_member[:person][:family_role] == 'child' if family_member[:person] && family_member[:person][:family_role]
        end

        if children
          eldest_child_age = 0
          children.each do |child|
            child[:person][:age] = Generators::GenerateAgeForStage.call(['infant', 'child', 'adolescent'].shuffle.first)
            eldest_child_age = child[:person][:age] if eldest_child_age < child[:person][:age]
          end

          # Give an age that is realistic given the children for the mother
          mother_age = rand(18..40) + eldest_child_age
          mother = household[:family_members].find { |family_member| family_member[:person][:family_role] == 'mother' }
          mother[:person][:age] = mother_age if mother

          # Give husband an age that is relatively close to the mother
          father_age = mother_age + rand(-6..6)
          father_age = 18 if father_age < 18
          father = household[:family_members].find { |family_member| family_member[:person][:family_role] == 'father' }
          father[:person][:age] = father_age if father
        end
      end
    end
  end
end