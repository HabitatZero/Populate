module Generators
  class GenerateDiets
    # Defaults are according to 2015 US Census
    def self.call(count:, vegan_percentage: 2, vegetarian_percentage: 3, pescatarian_percentage: 3)
      # TODO: Fix percentages for lower than 100
      vegan_count = count * vegan_percentage/100.to_f
      vegetarian_count = count * vegetarian_percentage/100.to_f
      pescatarian_count = count * pescatarian_percentage/100.to_f

      diets = []
      diets += ["vegan"] * vegan_count
      diets += ["vegetarian"] * vegetarian_count
      diets += ["pescatarian"] * pescatarian_count
      diets += ["omnivore"] * (count - vegan_count - vegetarian_count - pescatarian_count)
      diets.shuffle
    end
  end
end
