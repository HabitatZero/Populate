module Output
  class PrintToCSV
    def self.call(data)
      headers = [
        "first_name",
        "last_name",
        "age",
        "gender",
        "diet",
        "marital_status",
        "household_id",
        "household_name",
        "household_role",
        "water_consumption_profile_type",
        "water_consumption_profile_type_daily_consumption",
      ]

      rows = []
      data.each_with_index do |household, index|
        household[:family_members].each do |family_member|
          rows << [
            family_member[:person][:first_name],
            family_member[:person][:last_name],
            family_member[:person][:age],
            family_member[:person][:gender],
            family_member[:person][:diet],
            family_member[:person][:marital_status],
            index,
            household[:family_name],
            family_member[:person][:family_role],
            family_member[:person][:water_consumption_profile][:type],
            family_member[:person][:water_consumption_profile][:daily_consumption],
          ]
        end
      end

      puts headers.join(',')
      rows.each do |row|
        puts row.join(',')
      end
    end
  end
end
