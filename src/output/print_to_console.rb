module Output
  class PrintToConsole
    def self.call(data)
      # households_with_persons.sort_by! { |household| household[:family_name]}
      data.each do |household|
        puts "#{household[:family_name]}:"
        household[:family_members].each do |family_member|
          person = family_member[:person]
          puts "#{person[:first_name]} #{person[:last_name]}, #{person[:gender]}, #{person[:age]}, #{person[:family_role]}, has diet #{person[:diet] || 'omnivore'}, consumes #{person[:water_consumption_profile][:daily_consumption]} liters of water per day"
        end
        puts "\r\n"
      end
    end
  end
end
