require 'json'

module Output
  class PrintToJSON
    def self.call(data)
      puts JSON.generate(data)
    end
  end
end
