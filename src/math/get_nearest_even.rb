module Math
  class GetNearestEven
    def self.call(number)
      return number if number % 2 == 0
      return 2 if number == 1
      return number - 1
    end
  end
end
