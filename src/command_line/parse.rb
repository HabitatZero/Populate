module CommandLine
  class Parse
    def self.call(arguments)
      # Default arguments
      settings = {
        show_version: false,
        show_help: false,
        count: 10,
        sex_ratio: '1:1',
      }

      # Process command line arguments
      arguments.each_with_index do |arg, index|
        settings[:show_version] = true if ['-v', '--version'].include? arg
        settings[:show_help] = true if ['-h', '--help'].include? arg
        settings[:count] = arguments[index + 1].to_i if ['-c', '--count'].include? arg
        settings[:sex_ratio] = arguments[index + 1] if ['-sr', '--sex-ratio'].include? arg
        settings[:print_to_json] = true if ['-j', '--json'].include? arg
        settings[:print_to_csv] = true if ['--csv'].include? arg
      end
      settings
    end
  end
end
