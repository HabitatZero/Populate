# Populate

## Version 1.0.0

- [x] Show version and help text
- [x] Randomly generate names
- [x] Specify sex ratio and generate sex ratio specified amount of persons that are either M/F (to look into additional or replacing by broader gender-based generation, with intersex and non-binary, etc.)
- [x] Randomly assign diets (to look into more specific and inclusive diets later, as well as allergies and medical conditions)
- [x] Group persons in households (from 1 to a possible upper bound)
- [x] Generate ages that make sense with households
- [x] Assign water consumption profiles
- [ ] Add tests and coverage report
- [x] Output to CSV, JSON
- [ ] Update README
