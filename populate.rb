require 'pry'
require './src/command_line'
require './src/generators'
require './src/output'

settings = CommandLine::Parse.call(ARGV)
if settings[:show_version]
  puts 'Populate version 1.0.0'
  abort
end

if settings[:show_help]
  puts <<~HEREDOC
    Usage: ruby populate.rb [OPTIONS]

    Options:
      -h, --help          Show this help text
      -v, --version       Show version
      -c, --count         Set count of persons to generate (default 10)
      -sr, --sex-ratio    Specify sex ratio, format is W:F (default 1:1)
      --csv               Output to the console in CSV format
      -j, --json          Output to the console in JSON format
  HEREDOC
  abort
end

households = Generators::GeneratePopulation.call(settings)

if settings[:print_to_json]
  Output::PrintToJSON.call(households)
elsif settings[:print_to_csv]
  Output::PrintToCSV.call(households)
else
  Output::PrintToConsole.call(households)
end
