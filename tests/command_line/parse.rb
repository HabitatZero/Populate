require './tests/test_helper'

context "CommandLine::Parse" do
  test 'call' do
    results = CommandLine::Parse.call(['-c', '100'])

    refute results[:show_version]
    refute results[:show_help]
    assert results[:count] == 100
    assert results[:sex_ratio] == '1:1'
  end

  test 'call with all arguments' do
    results = CommandLine::Parse.call([
      '-v',
      '-c', '100',
      '-h',
      '-sr', '2:1',
      '-j',
      '--csv',
    ])

    assert results[:show_version]
    assert results[:show_help]
    assert results[:count] == 100
    assert results[:sex_ratio] == '2:1'
    assert results[:print_to_json]
    assert results[:print_to_csv]
  end
end
