require './tests/test_helper'

context "Generators::GenerateDiets" do
  test 'call' do
    results = Generators::GenerateDiets.call(count: 100)

    assert results.length == 100
  end
end
