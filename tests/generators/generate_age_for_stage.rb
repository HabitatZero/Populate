require './tests/test_helper'

context "Generators::GenerateAgeForStage" do
  test 'call for infant' do
    results = Generators::GenerateAgeForStage.call('infant')
    assert (results >= 0 && results <= 4)
  end

  test 'call for child' do
    results = Generators::GenerateAgeForStage.call('child')
    assert (results >= 5 && results <= 9)
  end

  test 'call for adolescent' do
    results = Generators::GenerateAgeForStage.call('adolescent')
    assert (results >= 10 && results <= 19)
  end

  test 'call for young adult' do
    results = Generators::GenerateAgeForStage.call('young_adult')
    assert (results >= 20 && results <= 29)
  end

  test 'call for adult' do
    results = Generators::GenerateAgeForStage.call('adult')
    assert (results >= 30 && results <= 39)
  end

  test 'call for early middle age' do
    results = Generators::GenerateAgeForStage.call('early_middle_age')
    assert (results >= 40 && results <= 49)
  end

  test 'call for late middle age' do
    results = Generators::GenerateAgeForStage.call('late_middle_age')
    assert (results >= 50 && results <= 65)
  end

  test 'call for senior' do
    results = Generators::GenerateAgeForStage.call('senior')
    assert (results >= 66 && results <= 99)
  end
end
