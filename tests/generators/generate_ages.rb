require './tests/test_helper'

context "Generators::GenerateAgeForStage" do
  test 'call' do
    results = Generators::GenerateAges.call(count: 10)
    assert results.length == 10
    assert results.select { |age| age[:sex] == 'female' }.length > 0
    assert results.select { |age| age[:sex] == 'male' }.length > 0
    adults = results.select { |age| age[:stage] == 'adult' }
    assert adults.length > 0
    assert adults.first[:age] >= 30 && adults.first[:age] <= 39
  end

  test 'call with no adults' do
    results = Generators::GenerateAges.call(
      count: 10,
      female_adult_percentage: 0,
      male_adult_percentage: 0,
    )

    assert results.length == 10
    assert results.select { |age| age[:stage] == 'adult' }.length == 0
  end

  test 'call with no men' do
    results = Generators::GenerateAges.call(
      count: 10,
      male_infant_percentage: 0,
      male_child_percentage: 0,
      male_adolescent_percentage: 0,
      male_young_adult_percentage: 0,
      male_adult_percentage: 0,
      male_early_middle_age_percentage: 0,
      male_late_middle_age_percentage: 0,
      male_senior_percentage: 0,
    )

    assert results.length == 10
    assert results.select { |age| age[:sex] == 'male' }.length == 0
  end

  test 'call with no women' do
    results = Generators::GenerateAges.call(
      count: 10,
      female_infant_percentage: 0,
      female_child_percentage: 0,
      female_adolescent_percentage: 0,
      female_young_adult_percentage: 0,
      female_adult_percentage: 0,
      female_early_middle_age_percentage: 0,
      female_late_middle_age_percentage: 0,
      female_senior_percentage: 0,
    )

    assert results.length == 10
    assert results.select { |age| age[:sex] == 'female' }.length == 0
  end
end
