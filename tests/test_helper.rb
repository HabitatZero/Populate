require 'pry'
require 'test_bench/activate'

require './src/calculators'
require './src/command_line'
require './src/generators'
require './src/output'
