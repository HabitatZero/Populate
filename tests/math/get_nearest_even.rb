require './tests/test_helper'

context "Math::GetNearestEven" do
  test 'call' do
    results = Math::GetNearestEven.call(1)
    assert results == 2
  end

  test 'call with even' do
    results = Math::GetNearestEven.call(2)
    assert results == 2
  end

  test 'call with 5' do
    results = Math::GetNearestEven.call(5)
    assert results == 4
  end
end
