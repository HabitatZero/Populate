require './tests/test_helper'

context "Calculators::CalculateCountFromSexRatio" do
  test 'call' do
    results = Calculators::CalculateCountFromSexRatio.call(count: 10, sex_ratio: '1:1')

    assert results[:first_half] == 5
    assert results[:second_half] == 5
  end

  test 'call with 2:1 ratio' do
    results = Calculators::CalculateCountFromSexRatio.call(count: 10, sex_ratio: '2:1')

    assert results[:first_half] == 7
    assert results[:second_half] == 3
  end

  test 'call with 300:300 ratio' do
    results = Calculators::CalculateCountFromSexRatio.call(count: 10, sex_ratio: '300:300')

    assert results[:first_half] == 5
    assert results[:second_half] == 5
  end

  test 'call with wrong ratio' do
    Calculators::CalculateCountFromSexRatio.call(count: 10, sex_ratio: '1:1:1')
  rescue RuntimeError => e
    assert e.message == "Format is wrong for sex ratio, it should be number followed by semicolon and then number, e.g. 1:1"
  end
end
